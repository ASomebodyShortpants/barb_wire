#include <Adafruit_GPS.h>
#include "adafruit_io.hpp"

namespace GPS
{
    // Connect to the GPS on the hardware I2C port
    Adafruit_GPS GPS(&Wire);

    uint32_t timer = millis();

    /**
     * @brief initialize GPS
     *
     * @note uses serial
     * @warning blocking operation; infinite loop if module not found
     *
     * @authors Adafruit, Frangel, Aaron
     */
    void initialize()
    {
        if (!GPS.begin(0x10))
            while (1)
                Serial.println("Error: No GPS detected.");
        GPS.sendCommand(PMTK_SET_NMEA_OUTPUT_RMCGGA);
        GPS.sendCommand(PMTK_SET_NMEA_UPDATE_1HZ);
        GPS.sendCommand(PGCMD_ANTENNA);
        Serial.println("GPS init!");
    }

    /**
     * @brief Send GPS datas
     *
     * @param emergency whether or not emergency services should be called
     * @param lat latitude of seizure
     * @param lon longitude of seizure
     * @param ele altitude/elevation of seizure
     * @return true :successful send
     * @return false :failed send
     */
    bool send(bool emergency)
    {
        Serial.println("Sending GPS data...");
        return AI::send(emergency, GPS::GPS.latitudeDegrees, GPS::GPS.longitudeDegrees, GPS::GPS.altitude);
    }
}