#include <Adafruit_ADXL345_U.h>
#define CAPACITY 200
/**
 * a fake queue
 *
 * i wanted to do templates but that's too hard
 *
 */
class faqueue
{
private:
    sensors_event_t data[CAPACITY];
    int first{0};
    int last{};

public:
    sensors_event_t front; //first element
    sensors_event_t end; //last element
    faqueue(int size);
    bool is_empty(sensors_event_t element);
    void push(sensors_event_t element);
    void pop();
    void p_p(sensors_event_t element);
    sensors_event_t stan_devs(float sum_x, float sum_y, float sum_z);
};
/**
 * @brief Construct a new faqueue<T>::faqueue object
 *
 * @param size starting size of the queue
 */
faqueue::faqueue(int size)
{
    last = size - 1;
    for (auto &d : data)
    {
        d.acceleration.x = d.acceleration.y = d.acceleration.z = 0;
    }
    front.acceleration.x = front.acceleration.y = front.acceleration.z = 0;
}
/**
 * @brief check if an event is empty aka all zeroes
 *
 * @param element event to check
 * @return true if empty false if not
 */
bool faqueue::is_empty(sensors_event_t element)
{
    if (element.acceleration.x == element.acceleration.y == element.acceleration.z == 0)
        return true;
    return false;
}
/**
 * @brief push element to back of the queue
 *
 * @param element the element to push
 * @warning will auto pop if more than CAPACITY (currently 200) elements, keep at 150 please
 */
void faqueue::push(sensors_event_t element)
{
    // circle back to front if exceed CAPACITY size, else just add one
    if (last == CAPACITY - 1)
        last = 0;
    else
        last++;
    // write input to storage array and last element access
    data[last] = end = element;

    if (first == last)
        // we exceeded the CAPACITY size of the array! pop the first element
        pop();
    if (is_empty(front)) // edge case during the first push operation
        front = data[first];
}
/**
 * @brief pop element at the forefront
 *
 */
void faqueue::pop()
{
    if (++first >= CAPACITY) // increment and see if we need to circle back to the front
        first = 0;
    front = data[first];
}
/**
 * @brief push element to the end before popping first element
 *
 * @param element the element to push
 */
void faqueue::p_p(sensors_event_t element)
{
    push(element);
    pop();
}
/**
 * @brief calculates the standard deviation for each of the axises
 * 
 * @param sum_x sum of the last 50 x axis values
 * @param sum_y sum of the last 50 y axis values
 * @param sum_z sum of the last 50 z axis values
 * @return sensors_event_t standard deviation values for x y and z
 */
sensors_event_t faqueue::stan_devs(float sum_x, float sum_y, float sum_z)
{
    sum_x /= 50, sum_y /= 50, sum_z /= 50; // turn sums into means
    sensors_event_t ans;
    for (auto &d : data)
    {
        ans.acceleration.x += (d.acceleration.x - sum_x) * (d.acceleration.x - sum_x);
        ans.acceleration.y += (d.acceleration.y - sum_y) * (d.acceleration.y - sum_y);
        ans.acceleration.z += (d.acceleration.z - sum_z) * (d.acceleration.z - sum_z);
    }
    ans.acceleration.x = sqrt(ans.acceleration.x), ans.acceleration.y = sqrt(ans.acceleration.y), ans.acceleration.z = sqrt(ans.acceleration.z);
    return ans;
}